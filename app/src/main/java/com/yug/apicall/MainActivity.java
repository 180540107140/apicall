package com.yug.apicall;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    Button activity_getcardbtn;
    TextView activity_tvdata;
    ImageView activity_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        init();

        process();

        listners();


        }

    private void init() {


        activity_getcardbtn = findViewById(R.id.activity_getcardbtn);
        activity_tvdata = findViewById(R.id.activity_tvdata);
        activity_image = findViewById(R.id.activity_image);

    }
    private void process() {
    }
    private void listners() {

        activity_getcardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                RequestQueue requestQueue;

                requestQueue = Volley.newRequestQueue(MainActivity.this);

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                        "https://deckofcardsapi.com/api/deck/new/draw/?count=1", null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            String success = response.getString("success");

                            if (success.equals("true")) {
                                JSONArray jsonArray = response.getJSONArray("cards");
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String a = jsonObject.getString("value");
                                String b = jsonObject.getString("suit");

                                String c = jsonObject.getString("image");

                                Log.d("myapp", "The Response is : " + a + " of " + b);
                                activity_tvdata.setText(a+" of "+b);

                                Bitmap bmImg = BitmapFactory.decodeFile(c);
                                activity_image.setImageBitmap(bmImg);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("myapp", "Something went wrong");
                    }

                });
                requestQueue.add(jsonObjectRequest);

            }
        });
    }
}